
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nelly
 */
public class Fisica {
      Scanner leer = new Scanner(System.in);
    
    private double p1;
    private double rp1;
    private double p2;
    private double rp2;
    private double p3;
    private double p4;
    private double rp4;
    private double ta1;
    private double ta2;
    private double ta3;
    private double ta4;
    private double ta5;
    private double pt;
    private double spt;
    private double q1;
    private double q2;
    private double q3;
    private double q4;
    private double q5;
    private double q6;
    private double q7;
    private double spq;
    private double prosqt;
    private double pq;
    private double prome;
    private double pa1;
    
    
    public void introducir(){
        
        System.out.print("Ingrese la nota del I parcial de física ");
        setP1(leer.nextDouble());
        setRp1(p1*0.25);
        System.out.println("El parcial I de física es: " + rp1 + "%");
        System.out.println("");
        System.out.print("Ingrese la nota del II parcial de física ");
        setP2(leer.nextDouble());
        setRp2(p2*0.25);
        System.out.println("El parcial II de física es: " + getRp2() + "%");
        System.out.println("");
        System.out.println("Ingrese su nota de las tareas de física: ");
        setTa1(leer.nextDouble());
        setTa2(leer.nextDouble());
        setTa3(leer.nextDouble());
        setTa4(leer.nextDouble());
        setTa5(leer.nextDouble());
        setSpt((ta1+ta2+ta3+ta4+ta5)/5);
        setPt(((ta1+ta2+ta3+ta4+ta5)/5)*0.50);
        
        System.out.println("La nota de tareas de física es de: " + getPt() + "%");
        System.out.println("");
        System.out.println("Ingrese la nota de los quizzes de física: ");
        setQ1(leer.nextDouble());
        setQ2(leer.nextDouble());
        setQ3(leer.nextDouble());
        setQ4(leer.nextDouble());
        setQ5(leer.nextDouble());
        setQ6(leer.nextDouble());
        setQ7(leer.nextDouble());
        setSpq((q1+q2+q3+q4+q5+q6+q7)/7);
        setPq(((q1+q2+q3+q4+q5+q6+q7)/7)*0.50);
        setProsqt((spt+spq)/2);
        
        System.out.println("La nota de los quizes de física es de: " + getPq() + "%");
        setP3((getPt() + getPq()) * 0.20);
        System.out.println("La nota del parcial III de física es de : " + p3 + "%");
        System.out.println("");
        System.out.print("Ingrese la nota del IV parcial de física ");
        setP4(leer.nextDouble());
        setRp4(p4*0.30);
        System.out.println("El parcial IV de física es: " + getRp4() + "%");
        prome = ((p1+p2+prosqt+p4)/4);
        System.out.println("");
        System.out.println("El promedio de física es de: "+prome);
        System.out.println("");
    }
    
    public Fisica(){

        p1=0;
        p2=0;
        p3=0;
        p4=0;
        ta1=0;
        ta2=0;
        ta3=0;
        ta4=0;
        ta5=0;
        q1=0;
        q2=0;
        q3=0;
        q4=0;
        q5=0;
        q6=0;
        q7=0;
        prome = 0;
    }
    
    public Fisica (int pa1, int rpp1,int pa2, int pa3, int pa4, int tm1, int tm2, int tm3, int tm4, int tm5, int qu1, int qu2, int qu3, int qu4, int qu5, int qu6, int qu7, int pm){
 
        p1=pa1;
        rp1=rpp1;
        p2=pa2;
        p3=pa3;
        p4=pa4;
        ta1=tm1;
        ta2=tm2;
        ta3=tm3;
        ta4=tm4;
        ta5=tm5;
        q1=qu1;
        q2=qu2;
        q3=qu3;
        q4=qu4;
        q5=qu5;
        q6=qu6;
        q7=qu7;
        prome=pm;
        
    }
    public void setP1(int pa1){
        this.setP1(pa1);
    }
    public int getP1(){
        return (int)p1;
    }
    /**

     * @return the p2
     */
    public void setRp1(int rpp1){
        this.setRp1(rpp1);
    }
    public int getRp1(){
        return (int)rp1;
    }
    public int getP2() {
        return (int)p2;
    }

    /**
     * @param p2 the p2 to set
     */
    public void setP2(int p2) {
        this.setP2(p2);
    }

    /**
     * @return the p3
     */
    public int getP3() {
        return (int)p3;
    }

    /**
     * @param p3 the p3 to set
     */
    public void setP3(int p3) {
        this.setP3(p3);
    }

    /**
     * @return the p4
     */
    public int getP4() {
        return (int) p4;
    }

    /**
     * @param p4 the p4 to set
     */
    public void setP4(int p4) {
        this.setP4(p4);
    }

    /**
     * @return the ta1
     */
    public int getTa1() {
        return (int) ta1;
    }

    /**
     * @param ta1 the ta1 to set
     */
    public void setTa1(int ta1) {
        this.setTa1(ta1);
    }

    /**
     * @return the ta2
     */
    public int getTa2() {
        return (int) ta2;
    }

    /**
     * @param ta2 the ta2 to set
     */
    public void setTa2(int ta2) {
        this.setTa2(ta2);
    }

    /**
     * @return the ta3
     */
    public int getTa3() {
        return (int) ta3;
    }

    /**
     * @param ta3 the ta3 to set
     */
    public void setTa3(int ta3) {
        this.setTa3(ta3);
    }

    /**
     * @return the ta4
     */
    public int getTa4() {
        return (int) ta4;
    }

    /**
     * @param ta4 the ta4 to set
     */
    public void setTa4(int ta4) {
        this.setTa4(ta4);
    }

    /**
     * @return the ta5
     */
    public int getTa5() {
        return (int) ta5;
    }

    /**
     * @param ta5 the ta5 to set
     */
    public void setTa5(int ta5) {
        this.setTa5(ta5);
    }

    /**
     * @return the q1
     */
    public int getQ1() {
        return (int) q1;
    }

    /**
     * @param q1 the q1 to set
     */
    public void setQ1(int q1) {
        this.setQ1(q1);
    }

    /**
     * @return the q2
     */
    public int getQ2() {
        return (int) q2;
    }

    /**
     * @param q2 the q2 to set
     */
    public void setQ2(int q2) {
        this.setQ2(q2);
    }

    /**
     * @return the q3
     */
    public int getQ3() {
        return (int) q3;
    }

    /**
     * @param q3 the q3 to set
     */
    public void setQ3(int q3) {
        this.setQ3(q3);
    }

    /**
     * @return the q4
     */
    public int getQ4() {
        return (int) q4;
    }

    /**
     * @param q4 the q4 to set
     */
    public void setQ4(int q4) {
        this.setQ4(q4);
    }

    /**
     * @return the q5
     */
    public int getQ5() {
        return (int)q5;
    }

    /**
     * @param q5 the q5 to set
     */
    public void setQ5(int q5) {
        this.setQ5(q5);
    }

    /**
     * @return the q6
     */
    public int getQ6() {
        return (int) q6;
    }

    /**
     * @param q6 the q6 to set
     */
    public void setQ6(int q6) {
        this.setQ6(q6);
    }

    /**
     * @return the q7
     */
    public int getQ7() {
        return (int) q7;
    }

    /**
     * @param q7 the q7 to set
     */
    public void setQ7(int q7) {
        this.setQ7(q7);
    }

    /**
     * @return the prome
     */
    public int getProme() {
        return (int) prome;
    }

    /**
     * @param prome the prome to set
     */
    public void setProme(int prome) {
        this.setProme(prome);
    }

    /**
     * @return the pa1
     */
    public int getPa1() {
        return (int) pa1;
    }

    /**
     * @param pa1 the pa1 to set
     */
    public void setPa1(int pa1) {
        this.setPa1(pa1);
    }

    /**
     * @param p1 the p1 to set
     */
    public void setP1(double p1) {
        this.p1 = p1;
    }

    /**
     * @param rp1 the rp1 to set
     */
    public void setRp1(double rp1) {
        this.rp1 = rp1;
    }

    /**
     * @param p2 the p2 to set
     */
    public void setP2(double p2) {
        this.p2 = p2;
    }

    /**
     * @return the rp2
     */
    public double getRp2() {
        return rp2;
    }

    /**
     * @param rp2 the rp2 to set
     */
    public void setRp2(double rp2) {
        this.rp2 = rp2;
    }

    /**
     * @param p3 the p3 to set
     */
    public void setP3(double p3) {
        this.p3 = p3;
    }

    /**
     * @param p4 the p4 to set
     */
    public void setP4(double p4) {
        this.p4 = p4;
    }

    /**
     * @return the rp4
     */
    public double getRp4() {
        return rp4;
    }

    /**
     * @param rp4 the rp4 to set
     */
    public void setRp4(double rp4) {
        this.rp4 = rp4;
    }

    /**
     * @param ta1 the ta1 to set
     */
    public void setTa1(double ta1) {
        this.ta1 = ta1;
    }

    /**
     * @param ta2 the ta2 to set
     */
    public void setTa2(double ta2) {
        this.ta2 = ta2;
    }

    /**
     * @param ta3 the ta3 to set
     */
    public void setTa3(double ta3) {
        this.ta3 = ta3;
    }

    /**
     * @param ta4 the ta4 to set
     */
    public void setTa4(double ta4) {
        this.ta4 = ta4;
    }

    /**
     * @param ta5 the ta5 to set
     */
    public void setTa5(double ta5) {
        this.ta5 = ta5;
    }
    /**
     * @param spt
     */
    /**
     * @return the pt
     */
    public double getPt() {
        return pt;
    }

    /**
     * @param pt the pt to set
     */
    public void setPt(double pt) {
        this.pt = pt;
    }
    /**
     * @param spt the spt to set
     */
    public void setSpt(double spt){
        this.spt = spt;
    }
    /**
     * @return the spt
     */
    public double getSpt(){
        return spt;
    }
    public void setQ1(double q1) {
        this.q1 = q1;
    }
    /**
     * @param q2 the q2 to set
     */
    public void setQ2(double q2) {
        this.q2 = q2;
    }

    /**
     * @param q3 the q3 to set
     */
    public void setQ3(double q3) {
        this.q3 = q3;
    }

    /**
     * @param q4 the q4 to set
     */
    public void setQ4(double q4) {
        this.q4 = q4;
    }

    /**
     * @param q5 the q5 to set
     */
    public void setQ5(double q5) {
        this.q5 = q5;
    }

    /**
     * @param q6 the q6 to set
     */
    public void setQ6(double q6) {
        this.q6 = q6;
    }

    /**
     * @param q7 the q7 to set
     */
    public void setQ7(double q7) {
        this.q7 = q7;
    }

    /**
     * @return the pq
     */
    public double getPq() {
        return pq;
    }
    
    /**
     * @param pq the pq to set
     */
    public void setPq(double pq) {
        this.pq = pq;
    }
    /**
     * @param spq the spq to set
     */
    public void setSpq(double spq){
        this.spq = spq;
    }
    
    /**
     * @return the spq
     */
    public double getSpq(){
        return spq;
    }
    
    /**
     * @param prosqt the prosqt to set
     * */
    public void setProsqt (double prosqt){
        this.prosqt = prosqt;
    }
    /**
     * @return the prosqt
     */
    public double getProsqt(){
        return prosqt;
    }
    /**
    * @param prome the prome to set
    */
    public void setProme(double prome) {
        this.prome = prome;
    }
    /**
     * @param pa1 the pa1 to set
     */
    public void setPa1(double pa1) {
        this.pa1 = pa1;
    }
}

    

