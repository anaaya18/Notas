/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nelly
 */
public class Alumno {
    
    private String nombre;
    private String apellido;
    private double numDoc;
    private int edad;
    
    public Alumno(){
        nombre="";
        apellido="";
        numDoc=0;
        edad=0;
    }
    
    public Alumno(String n,String p,int nd,int e){
        nombre=n;
        apellido=p;
        numDoc=nd;
        edad=e;
    }
    
            
    public void setNombre(String n){
        nombre = n;
    }
    public String getNombre(){
        return nombre;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @return the numDoc
     */
    public int getNumDoc() {
        return (int) numDoc;
    }

    /**
     * @param numDoc the numDoc to set
     */
    public void setNumDoc(int numDoc) {
        this.numDoc = numDoc;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
}
